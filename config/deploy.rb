$:.unshift(File.expand_path('./lib', ENV['rvm_path']))

#require "bundler/capistrano"

set :rvm_ruby_string, 'ruby-1.9.3-p194@faceswitch'

set :application, "faceswitch"
set :repository,  "git@bitbucket.org:maslutsk/faceswitch.git"

set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
set :deploy_to, "/var/www/sites/faceswitch.com"
set :user, "deploy"
#set :use_sudo, true

role :web, "107.21.212.163"                          # Your HTTP server, Apache/etc
role :app, "107.21.212.163"                          # This may be the same as your `Web` server
role :db,  "107.21.212.163", :primary => true # This is where Rails migrations will run

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

set :normalize_asset_timestamps, false


# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
 namespace :deploy do
   task :start do ; end
   task :stop do ; end
     task :restart, :roles => :app, :except => { :no_release => true } do
            run "touch #{File.join(current_path,'tmp','restart.txt')}"
      end
end