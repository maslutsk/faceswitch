var output = $('#output');
// Items to loop over
var loop = $('.navrow');
// Starting index
var index = loop.length-1;

function reset() {
    $('.navrow').css('background-color', 'transparent');
    //$('.navrow').removeClass('navselect');
    $('.navrow .navbutton').css('background-color', 'transparent');
    //$('.navrow .navbutton').removeClass('navselect');
}

function highlight(item) {
    reset();
    item.css('background-color','#ddd');
    //item.addClass('navselect');
}

var row_select = 0;
$(document)
    .keydown(function(e){
        // Only register enter keypresses
        if (e.which != 13) {
            return
        }
        // Switch to column scanning
        if (row_select == 0) {
            buttons = loop.eq(index).find('.navbutton');
            // Only one element, no need to iterate over them.
            if (buttons.length==1) {
                link = buttons.find('a');
                link.click();
                window.location.href = link.attr("href");
            } else {
                loop = buttons;
                index = loop.length - 1;
            }
        // Select item + switch back to row scanning
        } else if (row_select == 1) {
            // Select item

        } else {
            // Select item
            link = loop.eq(index).find('a');
            link.click();
            window.location.href = link.attr("href");
            // Switch back to row scanning
            loop = rows;
            index = loop.length - 1;
        }
        row_select = row_select + 1;
    });

setInterval(function () {
    // Go to next row, wrap around if at end of rows
    index = (index + 1) % loop.length;
    // Set current row to blue
    highlight(loop.eq(index));
}, 1000);