// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

var index;
var loop;
var keyboardShowing = false;
var keystrokes = 0

function reset() {
    $('.row').removeClass('navselect');
    $('.navrow').removeClass('navselect');
    $('.navrow .navbutton').removeClass('navselect');
    $('.row .navbutton').removeClass('navselect');
}

function highlight(item) {
    reset();
    //item.css('background-color','#00ff00');
    item.addClass('navselect');
}

function dispatch(button){

    if (button.attr('id') == "back") {
        history.back();
    } 

    else if (button.attr('id') == "refresh_button") {
        row_select = -1
        loop = $('.navrow');
        $("#refresh_button").hide();
    }

    else if (button.attr('id') == "post" || button.attr('id') == "add_comment") {
        //$("keyboardFrame").contentWindow.focus();
        popup('popUpDiv')
        row_select = -1
        loop = $('.row');            
    }

    else if (keyboardShowing){
        if (button.attr('id') == "enter") {
            //TODO perform post to wall here
            var currentText = $("textarea").html()
            var str = window.location.pathname;
            //alert(str.substr(1,100));
            $.ajax("/post?context="+str.substr(1,100)+"&message="+currentText);
            $("textarea").html("");
            row_select = -1;
            loop = $('.navrow');
            popup('popUpDiv');
        }
        else if (button.attr('id') == "quit"){
            row_select = -1
            loop = $('.navrow');
            popup('popUpDiv')
        }
        else if (button.attr('id') == "bksp"){
            var currentText = $("textarea").html()
            currentText = currentText.substring(0,currentText.length-1)
            $("textarea").html(currentText)
        }
        else if (button.attr('id') == "auto1" || button.attr('id') == "auto2" || button.attr('id') == "auto3"){
            var currentText = $("textarea").html()
            $("textarea").html(currentText.substring(0,currentText.length-keystrokes))
            $("textarea#message").append(button.html()); 
            $("textarea#message").append(" ");
            keystrokes = 0
            row_select = -1
            loop = $('.row'); 
        }
        else {
            $("textarea#message").append(button.attr('id'));
            keystrokes++
            updateAutoComplete($("textarea#message").val())
            row_select = -1
            loop = $('.row');
        }
    }
    else {
        link = button.find('a');
        link.click();
        window.location.href = link.attr("href");
    }
}

$(document).ready(function() {
    $('#refresh_button').hide();
    var output = $('#output');
    // Items to loop over
    loop = $('.navrow');
    // Starting index
    index = loop.length-1;

    $('#refresh_button').click(function()
    {
        row_select = -1
        loop = $('.navrow');
    });

    setInterval(function () {

    // Go to next row, wrap around if at end of rows
    index = (index + 1) % loop.length;
    // Set current row to blue
    highlight(loop.eq(index));
    }, 1000);
});

var row_select = 0;
$(document)
    .keydown(function(e){
        // Only register enter keypresses
        if (e.which != 13) {
            return
        }
        $('#refresh_button').show();
        // Switch to column scanning
        if (row_select == 0) {

            refresh_button = $('#refresh_button');
            if (!keyboardShowing) loop.eq(index).append(refresh_button);
            buttons = loop.eq(index).find('.navbutton');

            // Only one element, no need to iterate over them.
            if (buttons.length==1) {
                dispatch(buttons);
            } else {
                loop = buttons;
                index = loop.length - 1;
            } 
        // Select item + switch back to x`x`row scanning
        } else if (row_select == 1) {
            // Select item
            nav_button = loop.eq(index)
            dispatch(nav_button);
            index = loop.length - 1;
        } else {
            // Select item
            nav_button = loop.eq(index)
            dispatch(nav_button);
            index = loop.length - 1;
        }
        row_select = row_select + 1;
    });



function toggle(div_id) {
    var el = document.getElementById(div_id);
    if ( el.style.display == 'none' ) { el.style.display = 'block';}
    else {el.style.display = 'none';}
   
}


function popup(windowname) {
    toggle('blanket');
    toggle(windowname);
    keyboardShowing = !keyboardShowing; 
    $('#refresh_button').hide();
}


function updateAutoComplete(inputText, index) {
    var inputs = inputText.split(" ")
    var text = inputs[inputs.length-1]
    if (text.length == 0) {
        $("#auto1").html("Option1")
        $("#auto2").html("Option2")
        $("#auto3").html("Option3")
    }
    var count = 0
    for (var i = 0; i < words.length-1; i++){
        word = words[i]
        if (word.length >= text.length && word.substring(0,text.length) == text){
            if (count == 0) {
                $("#auto1").html(word)
            }
            else if (count == 1) {
                $("#auto2").html(word)
            }
            else if (count == 2) {
                $("#auto3").html(word)
            }
            else {
                return;
            }
            count++;
        }
    }
                
}


var words = ["the","and","that","was","for","with","his","have","not","they","this","had",
"are","but","from","she","which","you","one","all","were","her","would",
"there","their","will","when","who","him","been","has","more","out","can",
"what","said","about","other","into","than","its","time","only","could","new",
"them","man","some","these","then","two","first","may","any","like","now",
"such","make","over","our","even","most","state","after","also","made","many",
"did","must","before","back","see","through","way","where","get","much","well",
"your","know","should","down","work","year","because","come","people","just","say",
"each","those","take","day","good","how","long","own","too","little","use",
"very","great","still","men","here","life","both","between","old","under","last",
"never","place","same","another","think","house","while","high","right","might","came",
"off","find","states","since","used","give","against","three","himself","look","few",
"general","hand","school","part","small","american","home","during","number","again","mrs",
"around","thought","went","without","however","govern","don't","does","got","public","united",
"point","end","become","head","once","course","fact","upon","need","system","set",
"every","war","put","form","water","took","program","present","government","thing","told",
"possible","group","large","until","always","city","didn't","order","away","called","want",
"eyes","something","unite","going","face","far","asked","interest","later","show","knew",
"though","less","night","early","almost","let","open","enough","side","case","days",
"yet","better","nothing","tell","problem","toward","given","why","national","room","young",
"social","light","business","president","help","power","country","next","things","word","looked",
"real","john","line","second","church","seem","certain","big","four","felt","several",
"children","service","feel","important","rather","name","per","among","often","turn","development",
"keep","family","seemed","white","company","mind","members","others","within","done","along",
"turned","god","sense","week","best","change","kind","began","child","ever","law",
"matter","least","means","question","act","close","mean","leave","itself","force","study",
"york","action","it's","door","experience","human","result","times","run","different","car",
"example","hands","whole","center","although","call","five","inform","gave","plan","woman",
"boy","feet","provide","taken","thus","body","play","seen","today","having","cost",
"perhaps","field","local","really","increase","reason","themselves","clear","i'm","information","figure",
"late","above","history","love","girl","held","special","move","person","whether","college",
"sure","probably","either","seems","cannot","art","free","across","death","quite","street",
"value","anything","making","past","brought","moment","control","office","heard","problems","became",
"full","near","half","nature","hold","live","available","known","board","effect","already",
"economic","money","position","believe","age","together","shall","true","political","court","report",
"level","rate","air","pay","community","complete","music","necessary","society","behind","type",
"read","idea","wanted","land","party","class","organize","return","department","education","following",
"mother","sound","ago","nation","voice","six","bring","wife","common","south","strong",
"town","book","students","hear","hope","able","industry","stand","tax","west","meet",
"particular","cut","short","stood","university","spirit","start","total","future","front","low",
"century","washington","usually","care","recent","evidence","further","million","simple","road","sometimes",
"support","view","fire","says","hard","morning","table","left","situation","try","outside",
"lines","surface","ask","modern","top","peace","personal","member","minutes","lead","schools",
"talk","consider","gone","soon","father","ground","living","months","therefore","america","started",
"longer","dark","various","finally","hour","north","third","fall","greater","pressure","stage",
"expected","secretary","needed","that's","kept","eye","values","union","private","alone","black",
"required","space","subject","english","month","understand","i'll","nor","answer","moved","amount",
"conditions","direct","red","student","rest","nations","heart","costs","record","picture","taking",
"couldn't","hours","deal","forces","everything","write","coming","effort","market","island","wall",
"purpose","basis","east","lost","except","letter","looking","property","miles","difference","entire",
"else","color","followed","feeling","son","makes","friend","basic","cold","including","single",
"attention","note","cause","hundred","step","paper","developed","tried","simply","can't","story",
"committee","inside","reached","easy","appear","include","accord","actually","remember","beyond","dead",
"shown","fine","religious","continue","ten","defense","getting","central","beginning","instead","river",
"received","doing","employ","trade","terms","trying","friends","sort","administration","higher","cent",
"expect","food","building","religion","meeting","ready","walked","follow","earth","speak","passed",
"foreign","natural","medical","training","county","list","floor","piece","especially","indeed","stop",
"wasn't","england","difficult","likely","suddenly","moral","plant","bad","club","needs","international",
"working","countries","develop","drive","reach","police","sat","charge","farm","fear","test",
"determine","hair","results","stock","trouble","happened","growth","square","william","cases","effective",
"serve","miss","involved","doctor","earlier","increased","being","blue","hall","particularly","boys",
"paid","sent","production","district","using","thinking","concern","christian","press","girls","wide",
"usual","direction","feed","trial","walk","begin","weeks","points","respect","certainly","ideas",
"industrial","methods","operation","addition","association","combine","knowledge","decided","temperature","statement","yes",
"below","game","nearly","science","directly","horse","influence","size","showed","build","throughout",
"questions","character","foot","kennedy","firm","reading","husband","doubt","services","according","lay",
"stay","programs","anyone","average","french","spring","former","summer","bill","lot","chance",
"due","comes","army","actual","southern","neither","relate","rise","evening","normal","wish",
"visit","population","remain","measure","merely","arrange","condition","decision","account","opportunity","pass",
"demand","strength","window","active","deep","degree","ran","western","sales","continued","fight",
"heavy","arm","standard","generally","carry","hot","provided","serious","led","wait","hotel",
"opened","performance","maybe","station","changes","literature","marry","claim","works","bed","wrong",
"main","unit","george","hit","planning","supply","systems","add","chief","officer","soviet",
"pattern","stopped","price","success","lack","myself","truth","freedom","manner","quality","gun",
"manufacture","clearly","share","movement","length","ways","burn","forms","organization","break","somewhat",
"efforts","cover","meaning","progress","treatment","beautiful","placed","happy","attack","apparently","blood",
"groups","carried","sign","radio","dance","i've","regard","man's","train","herself","numbers",
"corner","reaction","immediately","language","running","recently","shake","larger","lower","machine","attempt",
"learn","couple","race","audience","middle","brown","date","health","persons","understanding","arms",
"daily","suppose","additional","hospital","pool","technical","served","declare","described","current","poor",
"steps","reported","sun","based","produce","determined","receive","park","staff","faith","responsibility",
"europe","latter","british","season","equal","learned","practice","green","writing","ones","choice",
"fiscal","term","watch","scene","activity","product","types","ball","heat","clothe","lived",
"distance","parent","letters","returned","forward","obtained","offer","specific","straight","fix","division",
"slowly","shot","poet","seven","moving","mass","plane","proper","propose","drink","obviously",
"plans","whatever","afternoon","figures","parts","approve","saying","born","immediate","fame","gives",
"extent","justice","cars","mark","pretty","opinion","ahead","glass","refuse","enter","completely",
"send","desire","judge","none","waiting","popular","democratic","film","mouth","corps","importance",
"touch","director","ship","there's","council","effects","event","worth","existence","designed","hardly",
"indicated","analysis","established","products","growing","patient","rule","bridge","pain","base","check",
"cities","elements","leaders","discussion","limited","sit","thomas","agreement","gas","factors","marriage",
"easily","closed","excite","accept","applied","allow","bit","married","oil","rhode","shape",
"interested","strange","compose","professional","remained","news","despite","beauty","responsible","wonder","spent",
"tear","unless","eight","permit","covered","negro","played","i'd","vote","balance","charles",
"loss","commission","original","fair","reasons","studies","exactly","built","behavior","enemy","teeth",
"bank","die","james","relations","weight","prepared","related","sea","bar","warn","post",
"trees","official","separate","clay","sunday","raised","events","thin","dropped","cattle","invite",
"playing","prevent","detail","standing","grow","places","someone","bright","talking","meant","print",
"capital","happen","sides","everyone","facilities","filled","lip","essential","techniques","june","knows",
"stain","hadn't","dinner","dog","dollars","caught","shout","buy","divide","entered","chicago",
"speed","jazz","appoint","governor","institutions","fit","materials","sight","store","dependence","explain",
"gain","he'd","leadership","quiet","realize","parents","communist","neighbor","round","included","kitchen",
"thousand","christ","isn't","radiation","broad","failure","retire","election","primary","king","books",
"command","edge","march","sitting","conference","bottom","lady","advise","churches","model","battle",
"giving","sport","address","considerable","spread","funds","trip","youth","construction","rock","regular",
"changed","boat","memory","successful","captain","hell","brother","murder","quick","moreover","highly",
"difficulty","inch","saw","clean","collect","camp","experiment","shows","authority","older","lord",
"variety","material","frame","distinguish","scientific","introduce","principal","jack","kill","collection","fell",
"entertain","pieces","management","otherwise","security","danger","entirely","civil","frequently","advertise","records",
"secret","title","impossible","yesterday","fast","mike","produced","favor","noted","caused","lose",
"purposes","solid","song","corporation","lie","winter","dress","electric","key","dry","reduce",
"fresh","goes","hill","names","slow","quickly","telephone","threaten","oppose","deliver","officers",
"expression","published","famous","pray","adopt","london","clothes","laws","citizens","announced","minute",
"master","sharp","advantage","greatest","relation","mary","leaving","gray","manager","animal","object",
"bottle","draw","honor","recognize","drop","intend","relationship","opposite","sources","poetry","ability",
"assistance","operating","bear","join","climb","companies","exist","fixed","gradual","possibility","hunt",
"spoke","satisfy","units","neck","sleep","doesn't","finished","carefully","facts","nice","practical",
"save","takes","allowed","wine","remind","rich","financial","dream","knife","stations","civilize",
"rose","cool","died","thick","imagine","literary","bind","inches","earn","familiar","seeing",
"distribution","marked","coffee","rules","slip","apply","page","beside","daughter","relatively","classes",
"explore","stated","german","musical","smile","significant","block","mix","reports","proposed","shelter",
"presence","affairs","named","ordinary","circumstances","mile","sweep","remains","admire","catholic","dust",
"operations","rain","tree","nobody","henry","robert","village","advance","offered","agree","mechanic",
"upper","occasion","requirements","capacity","appears","travel","article","houses","valley","beat","opening",
"box","evil","succeed","surround","application","slightly","remembered","interests","warm","subjects","search",
"presented","shoe","sweet","interesting","membership","suggest","notice","connection","extreme","exchange","flow",
"spend","everybody","poems","campaign","win","forced","freeze","nine","eat","newspaper","please",
"escape","lives","swim","file","wind","provides","shop","apartment","fashion","reasonable","created",
"germany","watched","cells","session","somehow","fully","whose","teacher","raise","recognized","unity",
"providence","reference","explained","twenty","russian","features","shoulder","sir","forest","studied","sam",
"signal","chair","reduced","procedure","forth","limit","disturb","universe","mentioned","pick","reality",
"differences","soft","traditional","mission","flat","looks","picked","weather","smaller","leg","chairman",
"ancient","narrow","fellow","twist","belief","excellent","rights","vocational","laid","politics","fill",
"response","struggle","disappear","prove","duty","follows","editor","welcome","anode","possess","hearing",
"buildings","ideal","scientist","formed","watching","circle","ought","garden","library","accuse","message",
"slight","junior","knock","empty","protection","treated","birth","expressed","planned","choose","confuse",
"virginia","killed","frighten","stayed","worry","surprise","aside","photograph","removed","turning","pull",
"personnel","agency","pointed","speech","listen","november","sample","louis","motor","selected","berlin",
"claims","spot","strike","increasing","exercise","handle","hole","leader","baby","ride","avoid",
"cross","twice","commercial","failed","prompt","fat","fourth","visitor","interior","jewish","wing",
"desk","faculty","forget","operate","stair","besides","relief","standards","france","perfect","pour",
"nevertheless"];



