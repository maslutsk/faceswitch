class ProfileController < ApplicationController

  require 'koala'


  # see this url https://github.com/arsduo/koala/wiki/Graph-API for api info

  def notifications

    if session[:access_token]
      @graph = Koala::Facebook::API.new(session[:access_token])
      @notifications = @graph.get_connections("me","notifications?include_read=true")
      if params[:offset]
        if params[:offset].to_i < 0
          @offset = 0
        end
        @offset = params[:offset].to_i
      else
        @offset = 0
      end
      @names = []
      @picture = []
      @message = []
      @story_id = []
      index = 0
      3.times do
        notification = @notifications[@offset+index]
        @names[index + @offset] = notification['from']['name']
        @picture[index + @offset] = @graph.get_picture(notification['from']['id'], :type => "large")
        @message[index + @offset] = notification['title']
        @story_id[index + @offset] = notification['object']['id']
        index += 1
      end

    end
  end

  def photos
    if session[:access_token]
      @graph = Koala::Facebook::API.new(session[:access_token])
      
      if params[:facebook_id]
        @photos = @graph.get_connections(params[:facebook_id], "photos")
      else
        @photos = @graph.get_connections("me", "photos")
      end


      if params[:offset]
        if params[:offset].to_i < 0
          @offset = 0
        end
        @offset = params[:offset].to_i
      else
        @offset = 0
      end

      puts "**** PRINT #{@photos[0]}"

      @picture = []
      @photo_id = []
      index = 0
      7.times do
        @picture[index] = @photos[@offset+index]['picture']
        @photo_id[index] = @photos[@offset+index]['id']
        index = index + 1
      end


    end
  end

  def friends
    if session[:access_token]
      begin
        @graph = Koala::Facebook::API.new(session[:access_token])
        friend_list = @graph.get_connections("me","friends")

        if params[:offset]
          if params[:offset].to_i < 0
            @offset = 0
          end
          @offset = params[:offset].to_i
        else
          @offset = 0
        end

        @picture = []
        @name = []
        @user_id = []
        index = 0
        7.times do
          @name[index] = friend_list[@offset+index]['name']
          @picture[index] = @graph.get_picture(friend_list[@offset+index]['id'], :type => "large")
          @user_id[index] = friend_list[@offset+index]['id']
          index = index + 1
        end
      rescue
      end
    end
  end

  def post
    @graph = Koala::Facebook::API.new(session[:access_token])
    context = params[:context]
    message = params[:message]

    puts "************************ NEW POST"
    puts "#{context}"
    puts "#{message}"

    if context.include? "content"
      @graph.put_connections
    else
      @graph.put_connections("me","feed",:message => message)
    end

  end

  def my_profile
    puts "********* TESTING ****************"
    if session[:access_token]
      
        @graph = Koala::Facebook::API.new(session[:access_token])

        if params[:facebook_id]
          @profile = @graph.get_object(params[:facebook_id])
          @picture = @graph.get_picture(params[:facebook_id], :type => "large")
          @allPhotos = @graph.get_connections(params[:facebook_id], "photos")
          @wall = @graph.get_connections(params[:facebook_id],"feed")
        else
          @profile = @graph.get_object("me")
          @picture = @graph.get_picture("me", :type => "large")
          @allPhotos = @graph.get_connections("me", "photos")
          @wall = @graph.get_connections("me","feed")
        end


        @photo_cover = @allPhotos[0]['source']
        puts "***#{@profile}"
        #puts "************************This is picture #{@allPhotos}"
        @name = @profile['name']
        @statuses = @graph.get_connection("me","statuses")
        @poster = @statuses[0]['from']['name']
        if @statuses
          @statuses.each do |status|
            puts "\n\n\n****** status: #{status}"
          end
        else 
          puts "\n\n\n**** status is nil"
        end
        @message = @statuses[0]['message']
        @likes = 0
        if @statuses[0]['likes']
          for like in @statuses[0]['likes']['data']
            @likes = (@likes+1)
          end
        end

        for wallpost in @wall
          if wallpost['message']
            puts "\n**********found wallpost: #{wallpost}"
            @firstpost = wallpost
            @firstpostpic = @graph.get_picture(wallpost['from']['id'])
            @firstpostbody = @firstpost['message'][0..100] + '...'
            if @firstpost['likes']
              @firstpostlikes = @firstpost['likes']['count']
            else
              @firstpostlikes = 0 # ['likes'] is nil if there are none...
            end
            if @firstpost['comments']
              @firstpostcomments = @firstpost['comments'].count
            end
            break    
          end

        end
    end
  end

  # this method is called from the view to set or update the access_token
  def set_oauth_access_token
    session[:access_token] = params[:facebook_access_token]
  end
end
#test
