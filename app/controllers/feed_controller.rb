class FeedController < ApplicationController

	require 'koala'

	def newsfeed
		if params[:offset]
			if params[:offset].to_i < 0
				@offset = 0
			end
			@offset = params[:offset].to_i
		else
			@offset = 0
		end

		if session[:access_token]
        	@graph = Koala::Facebook::API.new(session[:access_token])
			@newsfeed = @graph.get_connections("me", "home")
			@wallpost_id = ["","","","",""]
        	@messages = ["","","","",""]
        	@names = ["","","","",""]
        	@likes = [0,0,0,0,0]
        	@comments = [0,0,0,0,0]
        	@pics = ["","","","",""]
        	@count = 0
        	@catchup = 0
		
			for wallpost in @newsfeed
	          	if wallpost['message'] or wallpost['story']
	          		if (@catchup < @offset.to_i)
						@catchup = @catchup + 1 #Skip the first 'offset' posts
						next
					end
	          		@names[@count] = wallpost['from']['name']
	          		if (@names[@count].length > 16)
	          			@names[@count] = @names[@count][0..15] + '...'
	          		end
		            @pics[@count] = @graph.get_picture(wallpost['from']['id'])
		            if wallpost['message']
			            @messages[@count] = wallpost['message']
			            if (@messages[@count].length > 100)
			            	@messages[@count] = wallpost['message'][0..100] + '...'
			            end
			        else
			            @messages[@count] = wallpost['story']
			            if (@messages[@count].length > 100)
			            	@messages[@count] = wallpost['story'][0..100] + '...'
			            end
			        end
		        	if wallpost['likes']
		            	@likes[@count] = wallpost['likes']['count']
		        	end
		        	if wallpost['comments']
		            	@comments[@count] = wallpost['comments'].count
		        	end
		        	@wallpost_id[@count] = wallpost['id']
		        	@count = @count + 1
		        	if (@count == 5) 
		        		break
		        	end 
		        end   
		    end
        end      
	end


	def wall
		if params[:offset]
			if params[:offset].to_i < 0
				@offset = 0
			end
			@offset = params[:offset].to_i
		else
			@offset = 0
		end

		if session[:access_token]
        	@graph = Koala::Facebook::API.new(session[:access_token])
        	if params[:facebook_id]
				@newsfeed = @graph.get_connections(params[:facebook_id], "feed?limit=100")
			else
				@newsfeed = @graph.get_connections("me", "feed?limit=100")
			end
			@wallpost_id = ["","","","",""]
        	@messages = ["","","","",""]
        	@names = ["","","","",""]
        	@likes = [0,0,0,0,0]
        	@comments = [0,0,0,0,0]
        	@pics = ["","","","",""]
        	@count = 0
        	@catchup = 0
		
			for wallpost in @newsfeed
	          	if wallpost['message'] or (wallpost['story'] and wallpost['story'].include?("shared") and wallpost['story'].include?("photo"))
	          		if (@catchup < @offset.to_i)
						@catchup = @catchup + 1 #Skip the first 'offset' posts
						next
					end
	          		@names[@count] = wallpost['from']['name']
	          		if (@names[@count].length > 16)
	          			@names[@count] = @names[@count][0..15] + '...'
	          		end
		            @pics[@count] = @graph.get_picture(wallpost['from']['id'])
		            if wallpost['message']
			            @messages[@count] = wallpost['message']
			            if (@messages[@count].length > 100)
			            	@messages[@count] = wallpost['message'][0..100] + '...'
			            end
			        else
			            @messages[@count] = wallpost['story']
			            if (@messages[@count].length > 100)
			            	@messages[@count] = wallpost['story'][0..100] + '...'
			            end
			        end
		        	if wallpost['likes']
		            	@likes[@count] = wallpost['likes']['count']
		        	end
		        	if wallpost['comments']
		            	@comments[@count] = wallpost['comments'].count
		        	end
		        	@wallpost_id[@count] = wallpost['id']
		        	@count = @count + 1
		        	if (@count == 5) 
		        		break
		        	end 
		        end   
		    end
        end      
	end
end