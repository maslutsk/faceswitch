class ContentController < ApplicationController

	require 'koala'

	def single_content
	#defines @message @likes @comments @picture
		if session[:access_token] and params[:facebook_id]
      @graph = Koala::Facebook::API.new(session[:access_token])
			@content = @graph.get_object(params[:facebook_id])
			@user_id = @graph.get_object("me")['id']

			@content.each do |element|
				puts "**************#{element}\n\n\n"
			end

			if params[:offset]
				if params[:offset].to_i < 0
					@offset = 0
				end
				@offset = params[:offset].to_i
			else
				@offset = 0
			end

			@name = @content['from']['name']
    		@profile_picture =  @graph.get_picture(@content['from']['id'])
    		if @content['to']
    			@to_name = (@content['to']['data'])[0]['name']
    		end
			if @content['message']
				@message = @content['message'].length < 220 ? @content['message'] : @content['message'][0..219] + "..."
			elsif @content['story']
				@message = @content['story']
    		end
    		if @content['likes']
    			@likes = @content['likes']
    		end
    		if @content['comments']
    			@comments = @content['comments']
    		end
    		if @content['source']
    			@picture_source = (@content['source'])
    		end	
    		if @content['picture']
    			@picture = (@content['picture'])
    		end	
    		if @content['name']
    			@obj_name = @content['name']
    		else
    			@obj_name = ""
    		end
    		if @content['comments']
	    		if @content['comments']['data']
	    			@comments_count = @content['comments']['data'].length
	    		else
	    			@comments_count = 0
	    		end
	    	else
	    		@comments_count = 0
	    	end
    		if @content['likes']
    			@likes_count = @content['likes']['count']
    		else
    			@likes_count = 0
    		end

    		c = 0
    		userlikes = false
    		if @content['likes']
    			while c < @content['likes'].count
    				if @content['likes']['data'][c]['id'] == @user_id
    					userlikes = true
    				end
    				c = c + 1
    			end
    		end

    		if userlikes
    			@likebuttonpic = "single_content/unlike.png"
    		else
    			@likebuttonpic = "single_content/like.png"
    		end

    		@cmtcount = 0
    		i = @offset
    		@comments_from = ["",""]
    		@comments_text = ["",""]
    		@comments_likes = [0,0]
    		if @content['comments']
	    		if @content['comments']['data']
		    		while i < @offset+2 and i < @content['comments']['data'].length
		    			comment = @content['comments']['data'][i]
		    			@comments_from[@cmtcount] = (comment['from']['name'].length < 19) ? comment['from']['name'] : comment['from']['name'][0..18]+"..."
		    			@comments_text[@cmtcount] = comment['message']
		    			if comment['like_count']
		    				@comments_likes[@cmtcount] = comment['like_count']
		    			elsif comment['likes']
		    				@comments_likes[@cmtcount] = comment['likes']
		    			else
		    				@comments_likes[@cmtcount] = 0 
		    			end
		    			if comment['user_likes'] == "true"
		    				if @cmtcount == 0
		    					@comment1pic = "single_content/comment_unlike.png"
		    				else
		    					@comment2pic = "single_content/comment_unlike.png"
		    				end
		    			else
		    				if @cmtcount == 0
		    					@comment1pic = "single_content/comment_like.png"
		    				else
		    					@comment2pic = "single_content/comment_like.png"
		    				end
		    			end
		    			i = i + 1
		    			@cmtcount = @cmtcount + 1
		    		end
		    		if @cmtcount == 1
		    			@comment2pic = "single_content/comment_like.png"
		    		end
		    	else
		    		@comment1pic = "single_content/comment_like.png"
		    		@comment2pic = "single_content/comment_like.png"
		    	end
	    	else
	    		@comment1pic = "single_content/comment_like.png"
	    		@comment2pic = "single_content/comment_like.png"
		    end
      end      
	end
end